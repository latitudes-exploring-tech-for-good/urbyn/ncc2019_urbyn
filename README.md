# Urbyn

+ *Thème :* gestion des déchets.
+ *Phase d'avancement actuelle :* prototypage.
+ *Compétences associés :* IoT, conception app mobile, vision produit, développement web/mobile.

##### #0 | Résultats obtenus lors de la Nuit du Code Citoyen
Les agglomérations dépensent chaque jour beaucoup de temps et d'énergie à collecter les différentes bennes à déchets. Grâce à une technologie d'analyse du contenu de ces bennes, nous pouvons proposer des trajets optimisés et ainsi réduit le coût du recyclage.

##### #1 | Présentation de Urbyn

Créé fin 2017, Urbyn est une jeune société dont l’ambition est d’encourager les entreprises à être plus éco-responsable en leur proposant des services de recyclage simples, accessibles et sur-mesure.

Pour cela Urbyn développe :
+ Une place de marché qui permet de mettre en relation des entreprises détentrices de déchets avec des professionnels du recyclage locaux et indépendants.
+ Des outils de traçabilité et de suivi pour optimiser la valorisation de ses déchets et piloter les opérations de collecte.

En savoir plus : [urbyn.co](https://urbyn.co/).

##### #2 | Problématique : pesée connectée des biodéchets
Afin de réduire le gaspillage alimentaire, Urbyn souhaite proposer à ses clients dans l’hôtellerie restauration des outils de mesure de la quantité de déchets alimentaires jetés quotidiennement. Cela permettrait de chiffrer d’un point économique et environnemental les impacts liés au gaspillage alimentaire.

##### #3 | Le défi proposé
La mission consiste à mettre en place une corbeille de tri “connectée” mesurant le niveau de remplissage et la masse de déchets stockés. Le système de mesure serait connecté à une application de suivi des masses mesurées et à des indicateurs de performance (quantité de gaspillage par couvert, comparaisons etc …)

##### #4 | Livrables
Une poubelle “connectée” qui envoie des informations sur la masse et le niveau via le réseau lora à une application qui permet d’enregistrer les mesures. Le projet étant ambitieux, nous vous proposons deux catégories de livrables, à calibrer selon les avancées de l'équipe sur le hackathon, vos compétences et aspirations :
1. Assembler la poubelle connectée, la brancher via le réseau lora à une base SQL:

Les différents capteurs (masse et niveau) sont disponible, ainsi que du code pour récupérer ces messures. Le livrable consiste à :

    i. les intégrer à la poubelle connectée,
    
    ii. écrire un script permettant d'aggréger les deux types de mesures,
    
    iii. envoyer ces mesures via le réseau lora dans une base SQL (du code est déjà disponible pour envoyer des données jusqu'à une base SQL via lora)
    
Bonus

    iv. recommendations sur l'intégration des capteurs dans la poubelle (par exemple sur la protection des capteurs: prise en compte des intempéries, du contenu de la poubelle si le capteur est placé à l'intérieur ...)


2. Travailler sur le côté UX/UI du logiciel de visualisation des données : 

Les données des poubelles connectées vont être récupérées par Urbyn, afin d'analyser les tendances pour pouvoir faire des recommandations aux clients (exemple des restaurants: campagne d'analyse du gaspillage alimentaire), savoir à quelle fréquence les déchets doivent être ramassés ...
Le logiciel doit permettre de visualiser les données de manière adaptée, voire de lancer des alertes.

    i. lister les besoins de l'utilisateur (Urbyn),
    
    ii. les transformer en features et créer un parcours utilisateur,
    
    iii. réaliser des maquettes du logiciel,
    
Bonus

    iv. si l'équipe possède les compétences techniques ou souhaite découvrir ce domaine, commencer la réalisation du logiciel
    
    v. réfléchir à un système d'alertes

##### #5 | Ressources à disposition pour résoudre le défi
+ Urbyn dispose déjà d’un kit de composants électroniques composé de capteur de niveau, de capteur de masse, ainsi que d’un micro-contrôleur et un kit lora.
+ Urbyn met également à disposition des snippets de code déjà réalisés pour prendre les mesures de masse et niveau, et d'utilisation du kit lora.
+ Enfin nous mettrons également à disposition des poubelles en carton de 90 L et des objets de masse connue pour tester les capteurs.
+ Une base de données fictive de mesures pour la réalisation de du logiciel de visualisation de données.

##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long des 24h :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur.se.s de projets, notamment via la session de peer-learning ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur.se.s de projets d’avoir accès aux solutions les plus abouties possibles ;
+ Le transfert de méthodes et postures, qui pourront être appliquées et transmises aux équipes par les animateur.trice.s, les mentors ET les autres équipes ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur.se.s de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Julien : co-fondateur de Urbyn et porteur de projet. 
+ Yannick : co-fondateur de Latitudes.